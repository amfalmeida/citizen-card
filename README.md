# citizen-card

Read Portuguese Citizen Card without using Java Applet. This example uses websockets to check and display Citizen Card data on the webpage.

# Instruction:
+ Download the [citizen-card-server.zip] (http://www.amfalmeida.com/citizen-card/citizen-card-server.zip) file
+ Unzip the file 
+ Run citizen-card-server.exe file
+ The webpage [http://www.amfalmeida.com/citizen-card] (http://www.amfalmeida.com/citizen-card) will be opened automatically 
+ Insert a Citizen Card and see your data
